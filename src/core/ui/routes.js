"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// User
const routes_1 = require("user/routes");
const welcome_1 = require("core/ui/pages/welcome");
const App_1 = require("core/ui/App");
exports.routes = [
    {
        component: App_1.default,
        routes: [
            ...routes_1.userPublicRotes,
            {
                component: welcome_1.default,
                path: '/',
            },
        ],
    },
];
//# sourceMappingURL=routes.js.map