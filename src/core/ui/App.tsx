import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import renderRoutes from 'core/ui/renderRoutes';
import AppService from 'core/service/AppService';

interface IAppProps extends React.ClassAttributes<any> {
  route: any;
}

class App extends React.Component<IAppProps, any> {

  public componentWillMount(): void {
    console.log(AppService.getAuth());
  }

  public render() {
    const { route: { routes } } = this.props;
    return (
      <div>
        { renderRoutes(routes) }
      </div>
    );
  }
}

export default withRouter(App);
