"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_router_dom_1 = require("react-router-dom");
class WelcomePage extends React.Component {
    render() {
        return (React.createElement("h1", null,
            "Welcome:",
            React.createElement(react_router_dom_1.Link, { to: "/user" }, "To user list")));
    }
}
exports.default = WelcomePage;
//# sourceMappingURL=welcome.js.map