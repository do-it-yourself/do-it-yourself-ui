import * as React from 'react';
import { withRouter, Route, Link } from 'react-router-dom';

class WelcomePage extends React.Component<any> {


  public render() {
    return (
      <h1>
        Welcome:
        <Link to="/user">To user list</Link>
      </h1>
    );
  }

}

export default WelcomePage;
