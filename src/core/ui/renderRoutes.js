"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const Switch_1 = require("react-router/Switch");
const react_router_dom_1 = require("react-router-dom");
const renderRoutes = (routes, parentPath = '', extraProps = {}) => {
    return routes ? (React.createElement(Switch_1.default, null, routes.map((route, i) => {
        const absolutePath = (route.path) ? (parentPath !== '/' ? (parentPath + route.path) : route.path) : route.path;
        return (React.createElement(react_router_dom_1.Route, { key: route.key || i, path: absolutePath, exact: route.exact, strict: route.strict, render: (props) => React.createElement(route.component, Object.assign({}, props, extraProps, { route: route })) }));
    }))) : null;
};
exports.default = renderRoutes;
//# sourceMappingURL=renderRoutes.js.map