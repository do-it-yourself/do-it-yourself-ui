"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_router_dom_1 = require("react-router-dom");
const renderRoutes_1 = require("core/ui/renderRoutes");
const AppService_1 = require("core/service/AppService");
class App extends React.Component {
    componentWillMount() {
        console.log(AppService_1.default.getAuth());
    }
    render() {
        const { route: { routes } } = this.props;
        return (React.createElement("div", null, renderRoutes_1.default(routes)));
    }
}
exports.default = react_router_dom_1.withRouter(App);
//# sourceMappingURL=App.js.map