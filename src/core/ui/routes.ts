// User
import {userPublicRotes} from 'user/routes';

import WelcomePage from 'core/ui/pages/welcome';
import App from 'core/ui/App';

export const routes = [
  {
    component: App,
    routes: [
      ...userPublicRotes,
      {
        component: WelcomePage,
        path: '/',
      },
    ],
  },
];
