"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const LOGIN_URL = `/oauth/token`;
class AppService {
    static getWelcomeMessage() {
        return axios_1.default.get('/api', {
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        })
            .then(({ data }) => data)
            .catch(({ error }) => error);
    }
    static getAuth() {
        return axios_1.default.post(LOGIN_URL, null, {
            params: {
                grant_type: 'client_credentials',
            },
        })
            .then(({ data }) => data)
            .catch(({ error }) => error);
    }
}
exports.default = AppService;
//# sourceMappingURL=AppService.js.map