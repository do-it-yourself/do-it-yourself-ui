import axios from 'axios';

const LOGIN_URL = `/oauth/token`;


export default class AppService {

  public static getWelcomeMessage() {
    return axios.get('/api', {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      }
    )
      .then(({ data }) => data as string)
      .catch(({ error }) => error as string);
  }

  public static getAuth() {
    return axios.post(LOGIN_URL, null, {
        params: {
          grant_type: 'client_credentials',
        },
      }
    )
      .then(({ data }) => data as string)
      .catch(({ error }) => error as string);
  }
}