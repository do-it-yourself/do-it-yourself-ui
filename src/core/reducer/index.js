"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_1 = require("redux");
const react_router_redux_1 = require("react-router-redux");
const reducer_1 = require("user/reducer");
const rootReducer = redux_1.combineReducers({
    routing: react_router_redux_1.routerReducer,
    user: reducer_1.default,
});
exports.default = rootReducer;
//# sourceMappingURL=index.js.map