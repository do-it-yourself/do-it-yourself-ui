import UserBrowsePage from 'user/ui/BrowsePage';
import UserDetailsPage from 'user/ui/DetailsPage';


const publicRoutes = [
  {
    exact: true,
    component: UserBrowsePage,
    path: '/user',
  },
  {
    component: UserDetailsPage,
    path: '/user/:uuid',
  },
];

export {publicRoutes as userPublicRotes};
