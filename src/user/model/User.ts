
class User {
    public id: number;
    public uuid: string;
    public email: string;
    public name: string;
    public surname: string;
    public birthDate: number;
    public registrationDate: number;
    public phoneNumber: string;
    public roles: string[];
}

export default User;