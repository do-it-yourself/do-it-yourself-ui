"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const UrlTemplate = require("url-template");
const LIST_USERS_URL = '/api/user/list';
const GET_USER_URL = UrlTemplate.parse('/api/user/{uuid}');
class UserService {
    static listUsers() {
        const apiUrl = LIST_USERS_URL;
        return axios_1.default.get(apiUrl, {
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        }).then(({ data }) => data)
            .catch(({ error }) => error);
    }
    static getUser(uuid) {
        const apiUrl = GET_USER_URL.expand({ uuid });
        return axios_1.default.get(apiUrl, {
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        }).then(({ data }) => data)
            .catch(({ error }) => error);
    }
}
exports.default = UserService;
//# sourceMappingURL=UserService.js.map