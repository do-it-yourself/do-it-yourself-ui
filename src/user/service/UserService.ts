import axios from 'axios';
import * as UrlTemplate from 'url-template';
// Model
import User from 'user/model/User';

const LIST_USERS_URL = '/api/user/list';
const GET_USER_URL = UrlTemplate.parse('/api/user/{uuid}');

export default class UserService {

  public static listUsers() {

    const apiUrl = LIST_USERS_URL;

    return axios.get(apiUrl, {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      },
    ).then(({data}) => data as User[])
      .catch(({error}) => error as any);
  }

  public static getUser(uuid: string): Promise<User> {

    const apiUrl = GET_USER_URL.expand({uuid});

    return axios.get(apiUrl, {
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      },
    ).then(({data}) => data as User)
      .catch(({error}) => error as any);
  }

}
