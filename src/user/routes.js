"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BrowsePage_1 = require("user/ui/BrowsePage");
const DetailsPage_1 = require("user/ui/DetailsPage");
const publicRoutes = [
    {
        exact: true,
        component: BrowsePage_1.default,
        path: '/user',
    },
    {
        component: DetailsPage_1.default,
        path: '/user/:uuid',
    },
];
exports.userPublicRotes = publicRoutes;
//# sourceMappingURL=routes.js.map