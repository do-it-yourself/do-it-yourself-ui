"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Constants
const constants_1 = require("user/constants");
// Service
const UserService_1 = require("user/service/UserService");
const receiveUserAction = (user) => ({
    type: constants_1.RECEIVE_USER,
    payload: { user },
});
const receiveUsersAction = (users) => ({
    type: constants_1.RECEIVE_USERS,
    payload: { users },
});
exports.getUser = (uuid) => (dispatch) => {
    return UserService_1.default.getUser(uuid)
        .then((user) => dispatch(receiveUserAction(user)));
};
exports.listUsers = () => (dispatch) => {
    return UserService_1.default.listUsers()
        .then((users) => dispatch(receiveUsersAction(users)));
};
//# sourceMappingURL=public.js.map