// Constants
import {RECEIVE_USER, RECEIVE_USERS} from 'user/constants';
// Model
import User from 'user/model/User';
// Service
import UserService from 'user/service/UserService';


const receiveUserAction = (user: User) =>  ({
  type: RECEIVE_USER,
  payload: {user},
});

const receiveUsersAction = (users: User[]) =>  ({
  type: RECEIVE_USERS,
  payload: {users},
});

export const getUser = (uuid: string) => (dispatch) => {
  return UserService.getUser(uuid)
    .then((user) => dispatch(receiveUserAction(user)));
};

export const listUsers = () => (dispatch) => {
  return UserService.listUsers()
    .then((users) => dispatch(receiveUsersAction(users)));
};
