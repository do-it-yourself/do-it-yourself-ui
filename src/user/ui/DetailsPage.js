"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const redux_1 = require("redux");
// Action
const public_1 = require("user/action/public");
class UserDetailsPage extends React.Component {
    componentWillMount() {
        const { uuid, user, getUser } = this.props;
        if (!user || user.uuid !== uuid) {
            getUser(uuid);
        }
    }
    render() {
        const { user } = this.props;
        return user ? (React.createElement("div", null, `
          Email: ${user.email}
          Name: ${user.name}
          Surname: ${user.surname}
          Phone: ${user.phoneNumber}
          Roles: ${user.roles}
          Birth date: ${new Date(user.birthDate).getDate()}
          Registration date: ${new Date(user.registrationDate).getDate()}
          `)) : null;
    }
}
const mapStateToProps = (state, ownProps) => ({
    user: state.user.user,
    uuid: ownProps.match.params.uuid,
});
const mapDispatchToProps = (dispatch) => redux_1.bindActionCreators({
    getUser: public_1.getUser,
}, dispatch);
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(UserDetailsPage);
//# sourceMappingURL=DetailsPage.js.map