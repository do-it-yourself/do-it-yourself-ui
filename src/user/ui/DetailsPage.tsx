import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
// Action
import {getUser} from 'user/action/public';
// Model
import User from 'user/model/User';


interface IUserDetailsPage extends React.ClassAttributes<any> {
  user: User;
  getUser: (uuid) => void;
  uuid: string;
}

class UserDetailsPage extends React.Component<IUserDetailsPage> {

  public componentWillMount(): void {
    const {uuid, user, getUser} = this.props;

    if (!user || user.uuid !== uuid) {
      getUser(uuid);
    }
  }

  public render() {
    const {user} = this.props;
    return user ? (
      <div>
        {
          `
          Email: ${user.email}
          Name: ${user.name}
          Surname: ${user.surname}
          Phone: ${user.phoneNumber}
          Roles: ${user.roles}
          Birth date: ${new Date(user.birthDate).getDate()}
          Registration date: ${new Date(user.registrationDate).getDate()}
          `
        }
      </div>
    ) : null;
  }

}

const mapStateToProps = (state, ownProps) => ({
  user: state.user.user,
  uuid:  ownProps.match.params.uuid,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getUser,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(UserDetailsPage);
