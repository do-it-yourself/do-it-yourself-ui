import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
// Action
import { listUsers } from 'user/action/public';
// Model
import User from 'user/model/User';

interface IBrowsePageProps extends React.ClassAttributes<any> {
  listUsers: () => void;
  users: User[];
}

class UserBrowsePage extends React.Component<IBrowsePageProps> {

  public componentWillMount(): void {
    const { users, listUsers } = this.props;

    if (!users || users.length === 0) {
      listUsers();
    }
  }

  public render() {
    const { users } = this.props;

    return (
      <div>
        { users && users.length > 0 && users.map((user) => (
          <div key={ user.uuid }>
            <Link to={ `/user/${ user.uuid }` }>{ `${ user.email } - ${ user.uuid } - ${ user.roles }` }</Link>
          </div>
        )) }
      </div>
    );
  }

}


const mapStateToProps = (state, ownProps) => ({
  users: state.user.users,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  listUsers,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(UserBrowsePage);
