"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const redux_1 = require("redux");
const react_router_dom_1 = require("react-router-dom");
// Action
const public_1 = require("user/action/public");
class UserBrowsePage extends React.Component {
    componentWillMount() {
        const { users, listUsers } = this.props;
        if (!users || users.length === 0) {
            listUsers();
        }
    }
    render() {
        const { users } = this.props;
        return (React.createElement("div", null, users && users.length > 0 && users.map((user) => (React.createElement("div", { key: user.uuid },
            React.createElement(react_router_dom_1.Link, { to: `/user/${user.uuid}` }, `${user.email} - ${user.uuid} - ${user.roles}`))))));
    }
}
const mapStateToProps = (state, ownProps) => ({
    users: state.user.users,
});
const mapDispatchToProps = (dispatch) => redux_1.bindActionCreators({
    listUsers: public_1.listUsers,
}, dispatch);
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(UserBrowsePage);
//# sourceMappingURL=BrowsePage.js.map