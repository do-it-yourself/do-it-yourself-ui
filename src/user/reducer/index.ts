import update from 'immutability-helper';
// Constants
import { RECEIVE_USER, RECEIVE_USERS } from 'user/constants';
// Model
import User from 'user/model/User';

const initialState: {
  users: User[],
  user: User,
} = {
  users: [],
  user: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_USER: {
      const { user } = action.payload;
      const { users } = state;
      const updatedIndex = users && users.findIndex((userDetails) => userDetails.uuid === user.uuid) || -1;

      if (updatedIndex !== -1) {
        return update(state, {
          user: { $set: user },
          users: {
            [updatedIndex]: { $set: user },
          },
        });
      } else {
        return update(state, {
          user: { $set: user },
        });
      }
    }
    case RECEIVE_USERS: {
      const { users } = action.payload;
      return update(state, {
        users: { $set: users },
      });
    }
    default:
      return state;
  }
};

export default userReducer;
