"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const immutability_helper_1 = require("immutability-helper");
// Constants
const constants_1 = require("user/constants");
const initialState = {
    users: [],
    user: null,
};
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case constants_1.RECEIVE_USER: {
            const { user } = action.payload;
            const { users } = state;
            const updatedIndex = users && users.findIndex((userDetails) => userDetails.uuid === user.uuid) || -1;
            if (updatedIndex !== -1) {
                return immutability_helper_1.default(state, {
                    user: { $set: user },
                    users: {
                        [updatedIndex]: { $set: user },
                    },
                });
            }
            else {
                return immutability_helper_1.default(state, {
                    user: { $set: user },
                });
            }
        }
        case constants_1.RECEIVE_USERS: {
            const { users } = action.payload;
            return immutability_helper_1.default(state, {
                users: { $set: users },
            });
        }
        default:
            return state;
    }
};
exports.default = userReducer;
//# sourceMappingURL=index.js.map