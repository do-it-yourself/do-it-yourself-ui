"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const redux_1 = require("redux");
const react_redux_1 = require("react-redux");
const react_router_redux_1 = require("react-router-redux");
const createBrowserHistory_1 = require("history/createBrowserHistory");
const redux_thunk_1 = require("redux-thunk");
// Reducer
const reducer_1 = require("../src/core/reducer");
// Ui
const renderRoutes_1 = require("../src/core/ui/renderRoutes");
const routes_1 = require("../src/core/ui/routes");
const virtualPath = document.baseURI.replace(/^(https?:\/\/[^\/]+)?(.*)\/$/, '$2');
const historyOptions = { basename: `${virtualPath}` };
const history = createBrowserHistory_1.default(historyOptions);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || redux_1.compose;
const store = composeEnhancers(redux_1.applyMiddleware(redux_thunk_1.default, react_router_redux_1.routerMiddleware(history)))(redux_1.createStore)(reducer_1.default);
ReactDOM.render(React.createElement(react_redux_1.Provider, { store: store },
    React.createElement(react_router_redux_1.ConnectedRouter, { store: store, history: history }, renderRoutes_1.default(routes_1.routes))), document.getElementById('the-app'));
//# sourceMappingURL=App.js.map