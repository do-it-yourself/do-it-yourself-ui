"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const proxy = require("express-http-proxy");
const path = require("path");
const SSR_1 = require("./middleware/SSR");
const file_system_1 = require("file-system");
const app = express();
// @ts-ignore
const apiUrl = process.env.API_URL || 'http://localhost:8080';
const html = file_system_1.readFileSync(path.join('../assets', 'index.html'), { encoding: 'utf8' });
app.use((req, res, next) => {
    console.log('Incoming request, url:', req.url);
    next();
});
app.use('/api', proxy(apiUrl, {
    parseReqBody: false,
    timeout: 5000,
    proxyReqPathResolver: (req) => {
        let path = req.url;
        console.log(`HTTP proxy to ${apiUrl}${path}`);
        return '/api' + path;
    },
}));
app.use(express.static(path.join('../assets'), {
    etag: true,
    maxAge: '7d',
    index: false,
    redirect: false,
    immutable: true,
}));
app.use(SSR_1.render(html));
app.listen(3000, () => {
    console.log('HTTP server listening on: ' + 3000);
});
//# sourceMappingURL=index.js.map