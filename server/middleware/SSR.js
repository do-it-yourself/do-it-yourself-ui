"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const server_1 = require("react-dom/server");
const welcome_1 = require("../../src/core/ui/pages/welcome");
exports.render = (html) => (req, res) => {
    return res.set('Content-Type', 'text/html').send(renderView(html));
};
const renderView = (html) => {
    const initialView = (React.createElement(welcome_1.default, null));
    const renderedApp = server_1.renderToString(initialView);
    return html.replace("SERVER_HTML", renderedApp);
};
//# sourceMappingURL=SSR.js.map