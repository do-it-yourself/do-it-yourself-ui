import * as React from "react";
import { renderToString } from 'react-dom/server';

import WelcomePage from "../../src/core/ui/pages/welcome";


export const render = (html) => (req, res) => {


  return res.set('Content-Type', 'text/html').send(renderView(html));
};

const renderView = (html) => {
  const initialView = (<WelcomePage/>);

  const renderedApp = renderToString(initialView);
  return html.replace("SERVER_HTML", renderedApp);
}