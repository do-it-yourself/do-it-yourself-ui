const path = require('path');


module.exports = {

    mode: "production",
    target: "node",

    entry: {
        server: ['./server/index.ts']
    },
    output: {
        filename: '[name].js',
        path: path.join(process.cwd(), 'target/app/server'),
        publicPath: ''
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: ["awesome-typescript-loader"]
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        modules: [path.resolve('./src'), 'node_modules']
    },
    plugins: [
    ]
};
