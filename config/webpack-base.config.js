const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 3000;

const apiUrl = process.env.API_URL || 'http://localhost:8080';


module.exports = {

    mode: "development",

    devtool: "source-map",

    devServer: {
        port: port,
        host: host,
        historyApiFallback: {
            disableDotRule: true
        },
        proxy: {
            '/api': {
                target: apiUrl,
                ws: true,
            },
            '/oauth': {
                target: apiUrl,
                ws: true,
            }
        }
    },
    entry: {
       ui: ['./entrypoints/App.tsx']
    },
    output: {
        filename: '[name].js',
        path: path.join(process.cwd(), 'target/app/assets'),
        publicPath: ''
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.tsx?$/,
                loader: 'tslint-loader'
            },
            {
                test: /\.tsx?$/,
                use: ["awesome-typescript-loader"]
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        modules: [path.resolve('./src'), 'node_modules']
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Test app',
            filename: 'index.html',
            xhtml: true,
            template: './entrypoints/index.html',
        }),
    ]
};
